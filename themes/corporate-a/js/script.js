window.addEventListener("DOMContentLoaded", (event) => {

    /*----------------------------------------
    * loader
    */
    /* spinner */
    const elLoading = document.querySelector("#loader"); 
    window.addEventListener('load', (event) => {
        elLoading.classList.add('loaded');
    });

    //------------------------------------------------------------
    // メニューボタン
    const elNavToggler = document.getElementById("js-menu-toggler");
    const elNavBar = document.getElementById("js-navbar");
    elNavToggler.addEventListener("click", (e) => {
        elNavBar.classList.toggle("open");
    });
    
    //------------------------------------------------------------
    // お問い合わせ
    const btnSubmit = document.querySelector(".wpcf7-submit");
    const acceptances = Array.from(document.querySelectorAll(".wpcf7-acceptance"));
    if (btnSubmit && acceptances) {
        const changeBtnStatus = () => btnSubmit.disabled = acceptances.some((el) => !el.querySelector("[type=checkbox]").checked);
        acceptances.forEach((el) => el.querySelector("[type=checkbox]").addEventListener('change', changeBtnStatus));
        
        changeBtnStatus(); // 読み込み時に実行
    }

    /*----------------------------------------
    * smooth scroll in-page link
    */
    document.querySelectorAll(".pagelink").forEach(e => {
        e.addEventListener('click', e => {
            e.preventDefault();
            
            const selector = e.currentTarget.hash;
            const targetEl = document.querySelector(selector);
            const rectTop = targetEl.getBoundingClientRect().top; // 画面上部から要素まで
            const offsetTop = window.pageYOffset // 現在のスクロール量
            const buffer = 100;
            const top = rectTop + offsetTop - buffer;
            
            window.scrollTo({
            top,
            behavior: "smooth"
            });
        });
    });

    //------------------------------------------------------------
    // swiper発火
    if (document.querySelector('.swiper-container-fv')) {
        const swiper = new Swiper('.swiper-container-fv', {
            loop: true,
            centeredSlides: true,
            autoplay: {
                delay: 5000,
            },
            speed: 2000,
                effect: 'fade',
            fadeEffect: {
                crossFade: true
            },

            // lazyload
            preloadImages: false, // Disable preloading of all images
            lazy: {
                loadPrevNext: true,
            },
            slidesPerView: 1,
        });
    }
    if (document.querySelector('.swiper-container')) {
        const swiper = new Swiper('.swiper-container', {
            loop: true,
            centeredSlides: true,
            spaceBetween: 16,
            autoplay: {
                delay: 4500,
            },
            speed: 500,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // lazyload
            preloadImages: false, // Disable preloading of all images
            lazy: {
                loadPrevNext: true,
            },
            watchSlidesProgress: true,
            watchSlidesVisibility: true,
            
            // breakpoints
            slidesPerView: 1, // < 520px
            breakpoints: {
                // 780px <= w
                780: {
                    slidesPerView: 2,
                },
            }
        });
    }
});

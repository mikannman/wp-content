window.addEventListener("DOMContentLoaded", (event) => {

    //------------------------------------------------------------
    // section-gallery / gallery
    document.querySelectorAll(".gallery").forEach((gallery) => {

        // サムネイルとコンテンツのエレメント
        const galleryThumbs = gallery.querySelector(".gallery-thumbs");
        const galleryContents = gallery.querySelector(".gallery-contents");

        // UIエレメント
        const galleryUI = gallery.querySelector(".gallery-ui");
        const galleryBG = galleryUI.querySelector(".gallery-bg");
        const btnNext = galleryUI.querySelector(".gallery-btn-next");
        const btnPrev = galleryUI.querySelector(".gallery-btn-prev");
        
        // コンテンツと要素数
        const contents = galleryContents.querySelectorAll(".content");
        const contentsLength = contents.length;

        // data属性
        const getIndex = () => Number(galleryUI.dataset.index);
        const setIndex = (index) => galleryUI.dataset.index = index;
        const removeIndex = () => delete galleryUI.dataset.index;

        // モーダルを開く/閉じる処理
        const openContent = (newIndex) => {
            const index = getIndex();
            if (index == newIndex) return;
            // console.log('open! index='+index);

            // モーダルを閉じる
            //contents.forEach((content) => content.classList.remove("open"));
            if (!isNaN(index)) contents[index].classList.remove("open");
            // モーダルを開く
            contents[newIndex].classList.add("open");
            // dataset更新
            setIndex(newIndex);
            // UIを表示
            galleryUI.classList.add("show");
            // キーイベント登録
            document.body.addEventListener('keydown', keyEvents);
        };
        const closeContent = () => {
            const index = getIndex();
            // console.log('close! index='+index);

            // モーダルを閉じる
            if (!isNaN(index)) contents[index].classList.remove("open");
            // dataset削除
            removeIndex();
            // UIを閉じる
            galleryUI.classList.remove("show");
            // キーイベント削除
            document.body.removeEventListener('keydown', keyEvents);
        };
        // 次の/前のコンテンツを表示する処理
        const toNextContent = (e) => {
            const index = getIndex();
            const newIndex = (index + 1) % contentsLength;
            // console.log(`next! ${index} -> ${newIndex}, contentsLength=${contentsLength}`);
            openContent(newIndex);
        };
        const toPrevContent = (e) => {
            const index = getIndex();
            const newIndex = (index + contentsLength - 1) % contentsLength;
            // console.log(`prev! ${index} -> ${newIndex}, contentsLength=${contentsLength}`);
            openContent(newIndex);
        };
        const keyEvents = (e) => {
            // Do nothing if the event was already processed
            if (event.defaultPrevented) return; 
  
            if (e.key == 'ArrowRight') {
                toNextContent();
            } else if (e.key == 'ArrowLeft') {
                toPrevContent();
            }
        };

        // サムネクリックでコンテンツを表示
        galleryThumbs.querySelectorAll(".thumb").forEach((thumb, index) => {
            thumb.addEventListener('click', (e) => openContent(index));
        });

        // 背景/閉じるボタンでモーダルを閉じる
        galleryBG.addEventListener('click', closeContent);
        galleryContents.querySelectorAll(".btn-close").forEach(
            (btnClose) => btnClose.addEventListener('click', closeContent));

        // 次へ/前へボタンでコンテンツを切替
        btnNext.addEventListener('click', toNextContent);
        btnPrev.addEventListener('click', toPrevContent);
    });
});

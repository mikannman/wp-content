<?php get_header(); ?>

<?php get_template_part('inc/page-header', '', [
  'title' => 'ページタイトル',
  'subtitle' => 'ページサブタイトル',
  'jumbotron' => 'img/sample/jumbotron-sample.jpg',
]) ?>

<!-- page-section -->
<section class="section page-section">
  <div class="container">
    <h2 class="section-title heading-2">タイトル
      <div class="subtitle">サブタイトル</div>
    </h2>

    <p>本文が入ります。</p>

  </div><!-- /.container -->
</section>

<?php get_footer(); ?>
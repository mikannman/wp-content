<?php get_header(); ?>

<?php if ($wp_query->have_posts()) :
  while ($wp_query->have_posts()) : $wp_query->the_post();

    get_template_part('inc/page-header', '', [
      'title' => get_the_title() ?? '記事',
      // 'subtitle' => 'News',
      'jumbotron' => 'img/sample/jumbotron-sample.jpg',
    ]) ?>

    <!-- page-section -->
    <section class="section page-section">
      <div class="container">

        <article>
          <!-- 記事タイトル -->
          <h3 class="post-title heading-3">
            <div class="subtitle"><?= get_the_date('Y/m/d') ?></div><?= get_the_title() ?>
          </h3>
          <hr>

          <!-- 記事本文 -->
          <div class="post-content"><?= get_the_content() ?></div>
          <hr>

          <!-- カテゴリー -->
          <div class="post-category"><span class="text-black-3">カテゴリー</span>
            <?php foreach (get_the_category() as $category) : ?>
              <a href="<?= esc_url(get_category_link($category->term_id)) ?>"><?= $category->name ?></a>
            <?php endforeach; ?>
          </div>
        </article>

        <!-- ページネーション -->
        <ul class="wp-navi wp-navi-single">
          <li><?php previous_post_link('%link', '前の記事へ', TRUE); ?></li>
          <li><?php next_post_link('%link', '次の記事へ', TRUE); ?></li>
        </ul>

      </div><!-- /.container -->
    </section>
  <?php endwhile;
  else : ?>
  <li class="post">
    <div class="post-content">まだ記事はありません。</div>
  </li>
<?php endif;
wp_reset_postdata(); ?>

<?php get_footer(); ?>
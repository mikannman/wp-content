<?php get_header(); ?>

<!-- page-section -->
<section class="section page-section">
  <div class="container">

    <h3 class="heading-3">非対応ブラウザです</h3>

    <p>当Webサイトは Internet Explorer での閲覧に対応しておりません。<br>
      お手数ですが、</p>

    <ul class="ul">
      <li>Microsoft Edge</li>
      <li>Google Chrome</li>
    </ul>

    <p>等のブラウザをご利用の上ご覧ください。</p>

  </div><!-- /.container -->
</section>

<?php get_footer(); ?>
<!--============================== home-header ==============================-->
<!-- <div class="fv fv-bg-filter"> // 背景にフィルタ追加 -->
<div class="fv">
    <div class="container">
        <h2 class="fv-title heading-2">キャッチコピー</h2>
        <div class="fv-text">
            ここにコピーが入ります。<br>
            ここにコピーが入ります。ここにコピーが入ります。<br>
            ここにコピーが入ります。ここにコピーが入ります。
        </div>
    </div>

    <div class="swiper-container-fv">
        <div class="swiper-wrapper">

            <div class="swiper-slide">
                <img class="slide-image swiper-lazy" data-src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>">
            </div><!-- /.swiper-slide -->

            <div class="swiper-slide">
                <img class="slide-image swiper-lazy" data-src="<?= get_theme_file_uri('/img/sample/header.png') ?>">
            </div><!-- /.swiper-slide -->

            <div class="swiper-slide">
                <img class="slide-image swiper-lazy" data-src="<?= get_theme_file_uri('/img/sample/footer.png') ?>">
            </div><!-- /.swiper-slide -->

        </div><!-- /.swiper-wrapper -->
    </div><!-- /.swiper-container -->
</div>

<?php if (!hasLoad('slider')) : ?>
    <!-- script for swiper-slider -->
    <link rel="stylesheet" media="all" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.10/swiper-bundle.min.css">
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.10/swiper-bundle.min.js"></script>
<?php endif; ?>
<!--============================== /home-header ==============================-->
<!--============================== section-text ==============================-->
<section class="section section-text section-bg-filter">
  <div class="container">
    <!-- section-title -->
    <h2 class="section-title heading-2">タイトル
      <div class="subtitle">サブタイトル</div>
    </h2>
    <div class="section-message">ここにメッセージが入ります。</div>

    <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
    <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
    <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>

    <!-- button -->
    <div class="text-center mt-4">
      <a href="#" class="btn btn-rounded btn-2">詳細をみる</a>
    </div>

  </div><!-- /.container -->

  <!-- section-bg -->
  <img class="section-bg" src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>">
</section>
<!--============================== /section-text ==============================-->
<!--============================== section-cards ==============================-->
<section class="section section-image">
  <div class="container">
    <h2 class="section-title heading-2">タイトル
      <div class="subtitle">サブタイトル</div>
    </h2>
    <div class="section-message">ここにメッセージが入ります。</div>

    <div class="row mb-4">

      <div class="col-12 col-md-4 mb-3 mb-md-0">
        <div class="card">
          <img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>">
          <h4 class="heading-4">項目タイトル</h4>
          <p>項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。</p>
          <a href="#">＞詳細はこちら</a>
        </div>
      </div>

      <div class="col-12 col-md-4 mb-3 mb-md-0">
        <div class="card">
          <img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>">
          <h4 class="heading-4">項目タイトル</h4>
          <p>項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。</p>
          <a href="#">＞詳細はこちら</a>
        </div>
      </div>

      <div class="col-12 col-md-4 mb-3 mb-md-0">
        <div class="card">
          <img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>">
          <h4 class="heading-4">項目タイトル</h4>
          <p>項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。</p>
          <a href="#">＞詳細はこちら</a>
        </div>
      </div>

    </div><!-- /.row -->

    <div class="row">

      <div class="col-12 col-md-4 mb-3 mb-md-0">
        <div class="card">
          <h4 class="heading-4">項目タイトル</h4>
          <p>項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。</p>
          <a href="#">＞詳細はこちら</a>
        </div>
      </div>

      <div class="col-12 col-md-4 mb-3 mb-md-0">
        <div class="card">
          <h4 class="heading-4">項目タイトル</h4>
          <p>項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。</p>
          <a href="#">＞詳細はこちら</a>
        </div>
      </div>

      <div class="col-12 col-md-4 mb-3 mb-md-0">
        <div class="card">
          <h4 class="heading-4">項目タイトル</h4>
          <p>項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。項目の説明が入ります。</p>
          <a href="#">＞詳細はこちら</a>
        </div>
      </div>

    </div><!-- /.row -->

    <!-- button -->
    <div class="text-center mt-4">
      <a href="#" class="btn btn-rounded btn-2">詳細をみる</a>
    </div>

  </div><!-- /.container -->
</section>
<!--============================== /section-cards ==============================-->
<!--============================== page-header ==============================-->

<!-- jumbotron -->
<div class="jumbotron">
  <div class="container">
    <h1 class="jumbotron-title"><?= $args['title'] ?? '' ?>
      <?php if (isset($args['subtitle'])) : ?>
        <div class="subtitle"><?= $args['subtitle'] ?? '' ?></div>
      <?php endif; ?>
    </h1>
  </div>

  <?php if (isset($args['jumbotron'])) : ?>
    <img class="jumbotron-bg" src="<?= get_theme_file_uri($args['jumbotron']) ?>" alt="<?= $args['title'] ?? '' ?>" />
  <?php endif; ?>
</div>

<?php if (function_exists('bcn_display')) : ?>
  <!-- breadcrumbs -->
  <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
    <div class="container">
      <?php bcn_display() ?>
    </div>
  </div>
<?php endif; ?>

<!--============================== /page-header ==============================-->
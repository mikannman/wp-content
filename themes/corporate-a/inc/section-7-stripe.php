<!--============================== section-stripe ==============================-->
<section class="section section-stripe section-stripe-fixed" style="background-image:url('<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>');">
  <div class="container">
    <h3 class="stripe-text heading-3">背景固定の帯<br>(section-stripe-fixed)。</h3>
  </div><!-- /.container -->
</section>
<!--============================== /section-stripe ==============================-->

<!--============================== section-stripe ==============================-->
<section class="section section-stripe">
  <div class="container">
    <h3 class="stripe-text heading-3">背景なしの帯。</h3>
  </div><!-- /.container -->
</section>
<!--============================== /section-stripe ==============================-->

<!--============================== section-stripe ==============================-->
<section class="section section-stripe" style="background-image:url('<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>');">
  <div class="container">
    <h3 class="stripe-text heading-3">背景通常の帯。</h3>
  </div><!-- /.container -->
</section>
<!--============================== /section-stripe ==============================-->
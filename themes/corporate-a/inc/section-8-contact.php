<!--============================== section-contact ==============================-->
<section class="section section-contact section-contact-sp-bottom section-bg-filter">
  <div class="container">

    <!-- section-title -->
    <h2 class="section-title heading-2 text-center">お問い合わせ
      <div class="subtitle">Contact</div>
    </h2>
    <div class="section-message text-center mb-5">お見積り・ご相談など、なんでもお問い合わせください！</div>

    <div class="contacts">
      <!-- 電話番号 -->
      <div class="contact">
        <a href="tel:0000000000" class="contact-link">
          <span class="icon-circle icon-color-1"><img class="icon" width="24" height="24" src="<?= get_theme_file_uri('/img/icons/icon-tel.svg') ?>"></span>
          <span class="contact-text">00-0000-0000</span></a>
        <div class="contact-time">受付 9:00~18:00<br>(水曜/第1･3木曜除く)</div>
      </div>

      <!-- 問い合わせ -->
      <div class="contact">
        <a href="<?= home_url('contact') ?>" class="contact-link">
          <span class="icon-circle icon-color-1"><img class="icon" width="24" height="24" src="<?= get_theme_file_uri('/img/icons/icon-mail.svg') ?>"></span>
          <span class="contact-text">お問い合わせ</span></a>
        <div class="contact-time">24時間受付</div>
      </div>
    </div>

  </div><!-- /.container -->

  <!-- section-bg -->
  <img class="section-bg" src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>">
</section>
<!--============================== /section-contact ==============================-->
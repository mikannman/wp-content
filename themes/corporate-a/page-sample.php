<?php get_header(); ?>

<?php get_template_part('inc/page-header', '', [
  'title' => 'ページタイトル',
  'subtitle' => 'ページサブタイトル',
  'jumbotron' => 'img/sample/jumbotron-sample.jpg',
]) ?>

<!-- page-section -->
<section class="section page-section">
  <div class="container">
    <h2 class="section-title heading-2">タイトル
      <div class="subtitle">サブタイトル</div>
    </h2>

    <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
    <p>ここにテキストが入ります。<strong>強調する部分だよ！</strong>ここにテキストが入ります。<a href="#">リンクテキスト</a>もあるよ</p>

    <h3 class="heading-3">ボタン</h3>

    <ul class="ul">
      <li>リスト1</li>
      <li>リスト1</li>
      <li>リスト1</li>
    </ul>

    <ol class="ol">
      <li>リスト1</li>
      <li>リスト1</li>
      <li>リスト1</li>
    </ol>

    <h3 class="heading-3">説明（DL,DT,DD）</h3>
    <dl class="dl">
      <dt>項目名</dt>
      <dd>説明が入ります。説明が入ります。説明が入ります。</dd>
      <dt>項目名</dt>
      <dd>説明が入ります。説明が入ります。説明が入ります。</dd>
      <dt>項目名</dt>
      <dd>説明が入ります。説明が入ります。説明が入ります。</dd>
    </dl>


    <h3 class="heading-3">フロー</h3>
    <ol class="ol-flow">
      <li>にんにく、しょうがをみじん切りにする</li>
      <li>1.をバターと共に弱火でじっくり炒める</li>
      <li>玉ねぎを追加し、黄金色になるまで炒める</li>
      <li>各種スパイスを投入して炒める</li>
      <li>トマト缶と水を入れ、煮込む</li>
      <li>最後にガラムマサラを入れ、辛さを調整</li>
    </ol>

    <h3 class="heading-3">ボタン</h3>

    <div class="text-center mb-3">
      <h4 class="heading-4">普通のボタン</h4>
      <a class="btn btn-1 mb-3">ボタン1</a>
      <button type="button" class="btn btn-1 mb-3">ボタン2</button>
    </div>

    <div class="text-center mb-3">
      <h4 class="heading-4">キャンセルボタン</h4>
      <a class="btn btn-cancel mb-3">ボタン1</a>
      <button type="button" class="btn btn-cancel mb-3">ボタン2</button>
    </div>

    <div class="text-center mb-3">
      <h4 class="heading-4">メインカラーのボタン</h4>
      <a class="btn btn-2 mb-3">ボタン1</a>
      <button type="button" class="btn btn-2 mb-3">ボタン2</button>
    </div>

    <div class="text-center mb-3">
      <h4 class="heading-4">枠だけのボタン</h4>
      <a class="btn btn-2 btn-rounded mb-3">ボタン1</a>
      <button type="button" class="btn btn-2 btn-rounded mb-3">ボタン2</button>
    </div>

    <div class="text-center mb-3">
      <h4 class="heading-4">ボタンのサイズ</h4>
      <div>
        <a class="btn btn-1 btn-lg mb-3">ボタン1</a>
        <button type="button" class="btn btn-1 btn-lg mb-3">ボタン2</button>
      </div>
      <div>
        <a class="btn btn-1 btn-sm mb-3">ボタン1</a>
        <button type="button" class="btn btn-1 btn-sm mb-3">ボタン2</button>
      </div>
    </div>

    <h3 class="heading-3">カード</h3>
    <div class="row">
      <div class="col-12 col-md-4">
        <div class="card">
          <h4 class="heading-4">見出し</h4>
          <p>本文がここに入ります。本文がここに入ります。本文がここに入ります。本文がここに入ります。本文がここに入ります。本文がここに入ります。</p>
          <a href="#">詳細を見る</a>
        </div>
      </div>

      <div class="col-12 col-md-4">
        <div class="card">
          <h4 class="heading-4">見出し</h4>
          <p>本文がここに入ります。本文がここに入ります。本文がここに入ります。本文がここに入ります。本文がここに入ります。本文がここに入ります。</p>
        </div>
      </div>

      <div class="col-12 col-md-4">
        <div class="card">
          <p>本文がここに入ります。本文がここに入ります。本文がここに入ります。本文がここに入ります。本文がここに入ります。本文がここに入ります。</p>
          <a href="#">詳細を見る</a>
        </div>
      </div>
    </div>

    <h3 class="heading-3">表</h3>
    <h4 class="heading-4">標準のテーブル .table</h4>
    <table class="table">
      <thead>
        <tr>
          <th>見出し1</th>
          <th>見出し2</th>
          <th>見出し3</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>中身1</td>
          <td>中身2</td>
          <td>中身3</td>
        </tr>
        <tr>
          <td>中身4</td>
          <td>中身5</td>
          <td>中身6</td>
        </tr>
      </tbody>
    </table>

    <h4 class="heading-4">縞つきテーブル .table-stripe<br>カーソルでhover .table-hover</h4>
    <table class="table table-stripe table-hover">
      <thead>
        <tr>
          <th>見出し1</th>
          <th>見出し2</th>
          <th>見出し3</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>中身1</td>
          <td>中身2</td>
          <td>中身3</td>
        </tr>
        <tr>
          <td>中身4</td>
          <td>中身5</td>
          <td>中身6</td>
        </tr>
      </tbody>
    </table>

    <h4 class="heading-4">縦線つきテーブル .table-border<br>左右スクロール .table-responsive</h4>
    <table class="table table-border table-responsive">
      <thead>
        <tr>
          <th>見出し1</th>
          <th>見出し2</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>見出し</td>
          <td>中身に長い文字列が入った場合はこのように表示されます。中身に長い文字列が入った場合はこのように表示されます。</td>
        </tr>
        <tr>
          <td>見出し</td>
          <td>中身に長い文字列が入った場合はこのように表示されます。中身に長い文字列が入った場合はこのように表示されます。</td>
        </tr>
      </tbody>
    </table>

    <h3 class="heading-3">フォーム</h3>
    <h4 class="heading-4">ContactForm 7</h4>

    <div class="row">
      <div class="col-md-8 offset-md-2">
        <!-- Contact Form 7 のタグがそのまま使える -->
        <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
      </div>
    </div>

    <hr>
    <label>
      <input type="text" value="テキスト" placeholder="ここに入力">
    </label>

    <label>
      <input type="number" value="123" placeholder="ここに入力">
    </label>

    <label><input type="radio" value="1" name="radio">項目1</label>
    <label><input type="radio" value="2" name="radio">項目2</label>
    <label><input type="radio" value="3" name="radio">項目3</label>

    <select>
      <option>項目</option>
      <option>項目</option>
      <option>項目</option>
    </select>

    <textarea>テキストエリアの内容がここに入ります</textarea>


  </div><!-- /.container -->
</section>

<?php get_footer(); ?>
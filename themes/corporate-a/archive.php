<?php
$args = array(
  'post_type' => 'post',
  'post_status' => 'publish',
  'paged' => get_query_var('paged') ?: 1,
  'posts_per_page' => 10, // 1ページに表示する記事数。これを変更した場合、functions.phpのset_pre_get_posts()も変更
  'cat' => $cat, // $cat: 現在表示されているカテゴリIDが自動的に入る
);
$cat_name = get_category($cat)->name;
$excerpt_length = 50;

$wp_query = new WP_Query($args);

get_header(); ?>

<?php get_template_part('inc/page-header', '', [
  'title' => $cat_name ?? '記事一覧',
  // 'subtitle' => 'News',
  'jumbotron' => 'img/sample/jumbotron-sample.jpg',
]) ?>

<!-- page-section -->
<section class="section page-section">
  <div class="container">
    <ul class="posts">
      <?php if ($wp_query->have_posts()) :
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

          <li class="post">
            <h4 class="post-title heading-4">
              <div class="subtitle"><?= get_the_date('Y/m/d') ?></div><?= get_the_title() ?>
            </h4>
            <div class="post-excerpt"><?= get_my_excerpt($excerpt_length) ?></div>
            <a class="post-link" href="<?= get_permalink() ?>"></a>
          </li><!-- /.post -->

        <?php endwhile;
        else : ?>

        <li class="post">
          <div class="post-excerpt">まだ記事はありません。</div>
        </li>
    </ul>
  <?php endif; ?>

  <!-- ページネーション -->
  <ul class="wp-navi wp-navi-archive">
    <li><?php previous_posts_link('前のページへ'); ?></li>
    <li><?php next_posts_link('次のページへ'); ?></li>
  </ul>

  <?php wp_reset_postdata(); ?>

  </div><!-- /.container -->
</section>

<?php get_footer(); ?>
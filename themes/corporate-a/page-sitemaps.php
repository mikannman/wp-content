<?php get_header(); ?>

<?php get_template_part('inc/page-header', '', [
  'title' => 'サイトマップ',
  'subtitle' => 'Sitemap',
  'jumbotron' => 'img/sample/jumbotron-sample.jpg',
]) ?>

<!-- page-section -->
<section class="section page-section">
  <div class="container">

    <?php wp_nav_menu(['theme_location' => 'sitemap', 'container' => false]); ?>

  </div><!-- /.container -->
</section>

<?php get_footer(); ?>
</main>

<footer class="footer footer-bg-filter">
  <!-- footer-bg -->
  <img class="footer-bg" src="<?= get_theme_file_uri('/img/sample/footer.png') ?>">

  <div class="container">

    <div class="footer-top">
      <!-- .info -->
      <div class="info">
        <!-- logo -->
        <img class="info-logo-img" src="<?= get_theme_file_uri('/img/logo-white.png'); ?>" alt="<?= bloginfo('name') ?>" />
        <!-- 会社名 -->
        <h3 class="info-title text-large">株式会社サンプル</h3>
        <!-- 住所 -->
        <div class="info-address">
          〒000-0000<br>
          埼玉県さいたま市さいたま1-1-1<br>
          TEL: 000-0000-0000<br>
          FAX: 000-0000-0000
        </div>
      </div><!-- /.footer-top-->

      <!-- .menu -->
      <?php wp_nav_menu(['theme_location' => 'main-menu', 'container' => false]); ?>
    </div>

    <div class="footer-bottom">
      <!-- .menu -->
      <?php wp_nav_menu(['theme_location' => 'sub-menu', 'container' => false]); ?>

      <!-- .copyright -->
      <div class="copyright">Copyright &copy; 株式会社サンプル All rights reserved.</div>
    </div><!-- /.footer-bottom-->

  </div><!-- /.container -->
</footer>

<?php wp_footer(); ?>
</body>

</html>
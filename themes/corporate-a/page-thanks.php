<?php get_header(); ?>

<!-- page-section -->
<section class="section page-section">
  <div class="container">

    <h3 class="heading-3">お問い合わせありがとうございます</h3>

    <p>お問い合わせを受け付けました。<br>
      担当者が確認後、ご連絡させていただきます。</p>

    <a href="<?= home_url() ?>">トップページに戻る</a>

  </div><!-- /.container -->
</section>

<?php get_footer(); ?>
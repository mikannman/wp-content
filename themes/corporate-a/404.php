<?php get_header(); ?>

<!-- page-section -->
<section class="section page-section">
  <div class="container">

    <h3 class="heading-3">ページが見つかりません<div class="subtitle">404 NOT FOUND</div>
    </h3>

    <p>お探しのページは</p>
    <ul class="ul">
      <li>すでに削除されている・公開期間が終わっている</li>
      <li>アクセスしたアドレスが異なっている</li>
    </ul>
    <p>などの理由で見つかりませんでした。</p>
    <p>後者の場合、以下をご参照くださいませ。</p>
    <ul class="list">
      <li><a href="<?= home_url(); ?>">トップページ</a>から改めてリンクをたどる</li>
      <li><a href="<?= home_url('/sitemap'); ?>">サイトマップ</a>を参照する</li>
    </ul>

  </div><!-- /.container -->
</section>

<?php get_footer(); ?>
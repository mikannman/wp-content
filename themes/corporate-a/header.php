<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="<?= bloginfo('description'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php // loaderを使用する
  get_template_part('inc/loader') ?>
  <link rel="icon" href="<?= get_theme_file_uri('/img/favicon.ico') ?>">
  <link rel="apple-touch-icon" href="<?= get_theme_file_uri('/img/apple-touch-icon.png') ?>">
  <link rel="stylesheet" href="<?= get_theme_file_uri('/style.css') ?>" type="text/css" media="all">
  <script defer src="<?= get_theme_file_uri('/js/script.js') ?>"></script>
  <?php wp_head(); ?>
  <?php // IE未対応のお知らせ
  if (!is_page('ie-notice')) : ?>
    <script>
      var userAgent = window.navigator.userAgent.toLowerCase();
      if (userAgent.indexOf('msie') != -1 || userAgent.indexOf('trident') != -1) {
        location.href = "<?= home_url('/ie-notice') ?>";
      }
    </script>
  <?php endif; ?>
</head>

<body <?php body_class(); ?>>
  <!-- spinner -->
  <div id="loader" <?php if (is_page('ie-notice')) : ?>class="loaded" <?php endif; ?>>
    <div class="loader-spinner"></div>
    <div class="loader-text">Loading...</div>
  </div>

  <header>
    <nav id="js-navbar" class="navbar">
      <div class="container navbar-container">
        <div class="navbar-l">
          <a class="navbar-logo" href="<?= home_url() ?>"><img class="navbar-logo-img" src="<?= get_theme_file_uri('/img/logo.png'); ?>" alt="<?= bloginfo('name') ?>" /></a>
        </div>

        <div class="navbar-r">
          <!-- navbar-description -->
          <h1 class="navbar-description"><?= bloginfo('description'); ?></h1>

          <!-- menu-toggler -->
          <div id="js-menu-toggler" class="menu-toggler">
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
          </div>

          <!-- menu-items -->
          <?php
          // 管理画面（外観 > メニュー > メインメニュー）にて項目を設定。
          // 出力されたHTMLに置換して手動カスタマイズでもOK
          wp_nav_menu([
            'theme_location' => 'main-menu',
            'container' => false,
          ]) ?>
        </div>
      </div>
    </nav>
  </header>

  <main id="main">
    <?php // トップページのヘッダー
    if (is_home() || is_front_page()) get_template_part('inc/home-header'); ?>
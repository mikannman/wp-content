<?php get_header(); ?>

<?php get_template_part('inc/page-header', '', [
  'title' => 'お問い合わせ',
  'subtitle' => 'Contact',
  'jumbotron' => 'img/sample/jumbotron-sample.jpg',
]) ?>

<!-- page-section -->
<section class="section page-section">
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <p class="mb-4">下記のフォームからお気軽にお問い合わせください。</p>

        <!-- Contact Form 7 のタグを挿入 -->
        <?= do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
      </div>
    </div>

  </div><!-- /.container -->
</section>

<?php get_footer(); ?>
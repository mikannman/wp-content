<?php get_header() ?>

<!-- sections -->
<?php

get_template_part('inc/section-1-image');
get_template_part('inc/section-2-text');
get_template_part('inc/section-3-cards');
get_template_part('inc/section-4-banners');
get_template_part('inc/section-5-flow');
get_template_part('inc/section-6-boxes');
get_template_part('inc/section-7-stripe');
get_template_part('inc/section-8-contact');
get_template_part('inc/section-9-gallery');
get_template_part('inc/section-10-slider');
get_template_part('inc/section-11-posts');

?>
<!-- /sections -->

<?php get_footer(); ?>
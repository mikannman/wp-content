<!--============================== section-gallery ==============================-->
<section class="section section-gallery">
  <div class="container">
    <!-- section-title -->
    <h2 class="section-title heading-2 text-center">タイトル
      <div class="subtitle">サブタイトル</div>
    </h2>
    <div class="section-message text-center">ここにメッセージが入ります。</div>

    <!-- .slider -->
    <div class="slider slider-center mt-4 mb-5">
      <div class="swiper-container">
        <div class="swiper-wrapper">

          <?php
          $args = [
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 12, // 表示件数
            'category_name' => 'slider', // 表示するカテゴリのスラッグ名
            // 'cat' => 10, // または、カテゴリIDを指定
          ];
          $excerpt_length = 50; // 抜粋の文字数
          $wp_query = new WP_Query($args);

          if ($wp_query->have_posts()) :
            while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

              <div class="swiper-slide">
                <img class="slide-image swiper-lazy" data-src="<?= wp_get_attachment_image_src(get_post_thumbnail_id(), 'full')[0]; ?>">
                <div class="slide-description">
                  <h4 class="slide-title heading-4"><?= get_the_title() ?></h4>
                  <div class="slide-text"><?= get_my_excerpt($excerpt_length) ?></div>
                </div>
              </div><!-- /.swiper-slide -->

            <?php endwhile;
            else : ?>
            <div class="swiper-slide">
              表示できるスライドがありません。
            </div>
          <?php endif;
          wp_reset_postdata(); ?>

        </div><!-- /.swiper-wrapper -->
      </div><!-- /.swiper-container -->

      <div class="swiper-button-prev"></div>
      <div class="swiper-button-next"></div>
    </div><!-- /.slider -->

    <!-- button -->
    <div class="text-center mt-4">
      <a href="#" class="btn btn-2">詳細をみる</a>
    </div>

  </div><!-- /.container -->
</section>

<?php if (!hasLoad('slider')) : ?>
  <!-- script for swiper-slider -->
  <link rel="stylesheet" media="all" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.10/swiper-bundle.min.css">
  <script defer src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.10/swiper-bundle.min.js"></script>
<?php endif; ?>
<!--============================== /section-gallery ==============================-->
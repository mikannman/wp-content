<!--============================== section-posts ==============================-->
<section class="section section-posts">
  <div class="container">
    <!-- section-title -->
    <h2 class="section-title heading-2">お知らせ
      <div class="subtitle">News</div>
    </h2>

    <ul class="posts">
      <?php
      $args = [
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => 4, // 表示件数
        'category_name' => 'news', // 表示するカテゴリのスラッグ名
        // 'cat' => 5, // または、カテゴリIDを指定
      ];
      $excerpt_length = 50; // 抜粋の文字数
      $wp_query = new WP_Query($args);

      if ($wp_query->have_posts()) :
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

          <li class="post">
            <h4 class="post-title heading-4">
              <div class="subtitle"><?= get_the_date('Y/m/d') ?></div><?= get_the_title() ?>
            </h4>
            <div class="post-excerpt"><?= get_my_excerpt($excerpt_length) ?></div>
            <a class="post-link" href="<?= get_permalink() ?>"></a>
          </li><!-- /.post -->

        <?php endwhile;
        else : ?>
        <li class="post">
          <div class="post-content">まだお知らせはありません。</div>
        </li>
      <?php endif;
      wp_reset_postdata(); ?>
    </ul>

    <!-- button -->
    <div class="text-center mt-4">
      <a href="<?= home_url('/category/news') ?>" class="btn btn-rounded btn-2">お知らせ一覧</a>
    </div>

  </div><!-- /.container -->
</section>
<!--============================== /section-posts ==============================-->
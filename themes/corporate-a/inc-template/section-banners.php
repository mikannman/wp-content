<!--============================== section-banners ==============================-->
<section class="section section-banners">
  <div class="container">
    <h2 class="section-title heading-2">タイトル
      <div class="subtitle">サブタイトル</div>
    </h2>
    <div class="section-message">ここにメッセージが入ります。</div>

    <ul class="banners">
      <li>
        <a href="#"><img src="<?= get_theme_file_uri('/img/apple-icon-180x180.png') ?>"></a>
      </li>
      <li>
        <a href="#"><img src="<?= get_theme_file_uri('/img/apple-icon-180x180.png') ?>"></a>
      </li>
      <li>
        <img src="<?= get_theme_file_uri('/img/apple-icon-180x180.png') ?>">
      </li>
      <li>
        <img src="<?= get_theme_file_uri('/img/apple-icon-180x180.png') ?>">
      </li>
    </ul>

  </div><!-- /.container -->
</section>
<!--============================== /section-banners ==============================-->
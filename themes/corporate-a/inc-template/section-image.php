<!--============================== section-image ==============================-->
<section class="section section-image">
  <div class="container">
    <h2 class="section-title heading-2">タイトル
      <div class="subtitle">サブタイトル</div>
    </h2>
    <div class="section-message">ここにメッセージが入ります。</div>

    <div class="row">
      <div class="col-12 col-md-6 mb-3 mb-md-0">
        <img class="img-cover" src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>">
      </div>

      <div class="col-12 col-md-6">
        <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
        <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
        <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
      </div>
    </div><!-- /.row -->

  </div><!-- /.container -->
</section>
<!--============================== /section-image ==============================-->
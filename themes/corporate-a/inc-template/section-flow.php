<!--============================== section-flow ==============================-->
<section class="section section-flow">
  <div class="container">
    <h2 class="section-title heading-2">タイトル
      <div class="subtitle">サブタイトル</div>
    </h2>
    <div class="section-message">ここにメッセージが入ります。</div>

    <ol class="ol-flow">
      <li>にんにく、しょうがをみじん切りにする</li>
      <li>1.をバターと共に弱火でじっくり炒める</li>
      <li>玉ねぎを追加し、黄金色になるまで炒める</li>
      <li>各種スパイスを投入して炒める</li>
      <li>トマト缶と水を入れ、煮込む</li>
      <li>最後にガラムマサラを入れ、辛さを調整</li>
    </ol><!-- /.ol-flow -->

    <div class="text-center mt-4">
      <a href="#" class="btn btn-2">詳細をみる</a>
    </div>

  </div><!-- /.container -->
</section>
<!--============================== /section-flow ==============================-->
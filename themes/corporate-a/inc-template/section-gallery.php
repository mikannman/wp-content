<!--============================== section-gallery ==============================-->
<section class="section section-gallery">
  <div class="container">
    <!-- section-title -->
    <h2 class="section-title heading-2 text-center">タイトル
      <div class="subtitle">サブタイトル</div>
    </h2>
    <div class="section-message text-center">ここにメッセージが入ります。</div>

    <div class="gallery">
      <!-- gallery-thumbs -->
      <ul class="gallery-thumbs">
        <li class="thumb"><img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>"></li>
        <li class="thumb"><img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>"></li>
        <li class="thumb"><img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>"></li>
        <li class="thumb"><img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>"></li>
        <li class="thumb"><img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>"></li>
      </ul>

      <!-- gallery-contents -->
      <ul class="gallery-contents">

        <li class="content">
          <div class="row">
            <div class="col-12 col-md-6"><img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>"></div>
            <div class="col-12 col-md-6 mt-3 mt-md-0">
              <h3 class="heading-3">ギャラリータイトル1</h3>
              <p>1説明テキストが入ります。説明テキストが入ります。説明テキストが入ります。</p>
            </div>
          </div>
          <div class="btn-close">&times;</div><!-- 閉じるボタン -->
        </li><!-- /.content -->

        <li class="content">
          <div class="row">
            <div class="col-12 col-md-6"><img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>"></div>
            <div class="col-12 col-md-6 mt-3 mt-md-0">
              <h3 class="heading-3">ギャラリータイトル2</h3>
              <p>2説明テキストが入ります。説明テキストが入ります。説明テキストが入ります。</p>
            </div>
          </div>
          <div class="btn-close">&times;</div><!-- 閉じるボタン -->
        </li><!-- /.content -->

        <li class="content">
          <div class="row">
            <div class="col-12 col-md-6"><img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>"></div>
            <div class="col-12 col-md-6 mt-3 mt-md-0">
              <h3 class="heading-3">ギャラリータイトル3</h3>
              <p>3説明テキストが入ります。説明テキストが入ります。説明テキストが入ります。</p>
            </div>
          </div>
          <div class="btn-close">&times;</div><!-- 閉じるボタン -->
        </li><!-- /.content -->

        <li class="content">
          <div class="row">
            <div class="col-12 col-md-6"><img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>"></div>
            <div class="col-12 col-md-6 mt-3 mt-md-0">
              <h3 class="heading-3">ギャラリータイトル4</h3>
              <p>4説明テキストが入ります。説明テキストが入ります。説明テキストが入ります。</p>
            </div>
          </div>
          <div class="btn-close">&times;</div><!-- 閉じるボタン -->
        </li><!-- /.content -->

        <li class="content">
          <div class="row">
            <div class="col-12 col-md-6"><img src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>"></div>
            <div class="col-12 col-md-6 mt-3 mt-md-0">
              <h3 class="heading-3">ギャラリータイトル5</h3>
              <p>5説明テキストが入ります。説明テキストが入ります。説明テキストが入ります。</p>
            </div>
          </div>
          <div class="btn-close">&times;</div><!-- 閉じるボタン -->
        </li><!-- /.content -->

      </ul>

      <!-- gallery-ui -->
      <div class="gallery-ui">
        <div class="gallery-btn-next">&gt;</div>
        <div class="gallery-btn-prev">&lt;</div>
        <div class="gallery-bg"></div>
      </div>
    </div>

  </div>
</section>

<?php if (!hasLoad('gallery')) : ?>
  <!-- script for gallery -->
  <script defer src="<?= get_theme_file_uri('/js/gallery.js') ?>"></script>
<?php endif; ?>
<!--============================== /section-gallery ==============================-->

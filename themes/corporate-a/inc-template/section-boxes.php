<!--============================== section-boxes ==============================-->
<section class="section section-boxes">
  <div class="container">
    <h2 class="section-title heading-2">タイトル
      <div class="subtitle">サブタイトル</div>
    </h2>
    <div class="section-message">ここにメッセージが入ります。</div>

    <a class="boxes" href="#">
      <div class="box box-text">
        <h3 class="box-heading heading-3">項目名
          <div class="subtitle">サブタイトル</div>
        </h3>
      </div>

      <div class="box">
        <img class="box-image" src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>">
      </div>
    </a><!-- /.boxes -->

    <a class="boxes" href="#">
      <div class="box">
        <img class="box-image" src="<?= get_theme_file_uri('/img/sample/sample-bg.png') ?>">
      </div>

      <div class="box box-text">
        <h3 class="box-heading heading-3">項目名
          <div class="subtitle">サブタイトル</div>
        </h3>
      </div>
    </a><!-- /.boxes -->

  </div><!-- /.container -->
</section>
<!--============================== /section-boxes ==============================-->
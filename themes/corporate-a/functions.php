<?php
/* メモを見ながら、不要な部分は除いてください */

/*==========================================================================================
* テンプレート構成
*===========================================================================================*/

function register_my_menus()
{
  register_nav_menus(array(
    'main-menu' => 'メインメニュー',
    // 'footer-menu'  => 'フッターメニュー',
    'sub-menu'  => 'フッターサブメニュー',
    'sitemap' => 'サイトマップ',
  ));
}
add_action('after_setup_theme', 'register_my_menus');

/*==========================================================================================
* フロントエンド
*===========================================================================================*/

/*------------------------------------------------------------
* デフォルトのjQueryのかわりに、最新バージョンのjQueryをCDNから読み込む。
* また、フッターで読み込むことにより表示パフォーマンスを改善する。
*/
add_action('wp_print_scripts', function () {
  // 管理画面ではjQueryを削除できない。
  if (is_admin() || is_page('contact')) return;

  wp_deregister_script('jquery');
  wp_deregister_script('jquery-core');
});

/*------------------------------------------------------------
* ヘッダーの不要な要素を削除する。
*/
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('wp_head', 'wp_oembed_add_host_js');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'feed_links_extra', 3);

//WordPress 4.2 絵文字対応のスクリプトを無効化する
function disable_emoji()
{
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('admin_print_scripts', 'print_emoji_detection_script');
  remove_action('wp_print_styles', 'print_emoji_styles');
  remove_action('admin_print_styles', 'print_emoji_styles');
  remove_filter('the_content_feed', 'wp_staticize_emoji');
  remove_filter('comment_text_rss', 'wp_staticize_emoji');
  remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
}
add_action('init', 'disable_emoji');

//絵文字のDNSプリフェッチを削除する（絵文字を使わない場合のみ）
add_filter('emoji_svg_url', '__return_false');

/*------------------------------------------------------------
* titleタグが、ページ名とサイト名を両方含むように変更する処理
* https://www.webtoolnavi.com/wordpress-code-snippets/
* 例) 会社概要ページのタイトル -> "会社概要 | モデリング株式会社"
*/
function change_title($title)
{
  $separater = ' | '; //セパレーターを設定
  $blog_name = get_bloginfo('name'); //ブログ名を取得
  //詳細ページの場合
  if (is_single()) {
    $title = get_the_title() . $separater . $blog_name;
  }
  //固定ページの場合
  elseif (is_page()) {
    $title = get_the_title() . $separater . $blog_name;
  }
  //カテゴリーページの場合
  elseif (is_category()) {
    $title = single_cat_title('', false) . 'の一覧' . $separater . $blog_name;
  }
  //タグページの場合
  elseif (is_tag()) {
    $title = single_tag_title('', false) . 'の一覧' . $separater . $blog_name;
  }
  //タクソノミーページの場合
  elseif (is_tax()) {
    $title = single_term_title('', false) . 'の一覧' . $separater . $blog_name;
  }
  //アーカイブページの場合
  elseif (is_archive()) {
    $title = get_the_archive_title() . 'の一覧' . $separater . $blog_name;
  }
  return $title;
}
add_filter('pre_get_document_title', 'change_title');

//トップページにて、タイトルにサイトの説明文が表示されないようにする
//http://glatchdesign.com/blog/web/wordpress/1070
add_filter('document_title_parts', 'remove_title_description', 10, 1);
function remove_title_description($title)
{
  if (is_home() || is_front_page()) {
    unset($title['tagline']);
  }
  return $title;
}
//WP側でtitleタグを出力する設定を有効化
add_theme_support('title-tag');

/*
* 指定したキーの hasLoad 関数が実行されたことがあるかどうかを返す。
* 一度しか実行しない場合の判定に。
*/
$_hasLoadFlags = [];
function hasLoad($key)
{
  global $_hasLoadFlags;

  // $key は空ではダメ
  if (empty($key)) {
    trigger_error('hasLoad($key) に空の引数が渡されました。', E_USER_WARNING);
    return false;
  }
  // 定義済みなら実行済とみなす
  if (isset($_hasLoadFlags[$key])) {
    return true;
  }
  // 未定義なら定義し、未実行とみなす
  $_hasLoadFlags[$key] = 1;
  return false;
}

// 抜粋の文字数
function get_my_excerpt($excerpt_length)
{
  if (has_excerpt()) {
    return get_the_excerpt();
  }
  $content = get_the_content();
  return mb_strlen($content) > $excerpt_length ? mb_substr($content, 0, $excerpt_length) . '…' : $content;
}

// ページネーションの件数を設定
function set_pre_get_posts($query)
{
  if (is_admin() || !$query->is_main_query()) {
    return;
  }

  $query->set('posts_per_page', '10');
  return;
}
add_action('pre_get_posts', 'set_pre_get_posts');

/*==========================================================================================
* Contact Form 7
*===========================================================================================*/

/*------------------------------------------------------------
* Contact form 7を特定の場所でのみ有効
*/
add_filter('wpcf7_load_js', '__return_false');
add_filter('wpcf7_load_css', '__return_false');
function st_scripts()
{
  if (function_exists('wpcf7_enqueue_scripts') && is_page(['contact'])) {
    wpcf7_enqueue_scripts();
    wpcf7_enqueue_styles();
  }
}
add_action('wp_enqueue_scripts', 'st_scripts');

/*------------------------------------------------------------
* contact form 7 のフォーム送信が完了した時、
* 指定ページにリダイレクト & Googleタグマネージャにイベントとして送信
*/
function contactform7_gtm_hook()
{
  if (!is_page('contact')) return;

  // contactform 7 のIDを指定
  $contactform_id = 0;

  // フォーム送信後に遷移するページのパスを指定
  $fallback_url = './thanks';

  $output = "<script>
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      if ( '{$contactform_id}' == event.detail.contactFormId ) {
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({'event':'contactform7_sent', 'contactform7_type':'お問い合わせ'});
      }
      location = '{$fallback_url}';
    }, false );
  </script>";
  echo $output; //定義された内容を出力します
}
add_action('wp_footer', 'contactform7_gtm_hook'); //「contactform7_gtm_hook」関数を<head>内で実行します

/*==========================================================================================
* 管理画面
*===========================================================================================*/

/*------------------------------------------------------------
* 管理バーを非表示にする。
*/
function mytheme_kill_admin_bar()
{
  return false;
}
add_filter('show_admin_bar', 'mytheme_kill_admin_bar', 1000);

//投稿時にアイキャッチ画像を設定する
add_theme_support('post-thumbnails');

/*------------------------------------------------------------
* 投稿画面で入れ子になったカテゴリ選択ボックスの階層を保つ
*/
function solecolor_wp_terms_checklist_args($args, $post_id)
{
  if ($args['checked_ontop'] !== false) {
    $args['checked_ontop'] = false;
  }
  return $args;
}
add_filter('wp_terms_checklist_args', 'solecolor_wp_terms_checklist_args', 10, 2);

/*------------------------------------------------------------
* 保存するリビジョン数の指定
* リビジョン数はDBの行数を圧迫し、パフォーマンス低下に繋がるため上限を設けるとよい。
* すべてのカスタム投稿タイプに反映される模様。
*/
function set_revisions_number()
{
  return 5;
}
add_filter('wp_revisions_to_keep', 'set_revisions_number');

/*------------------------------------------------------------
* 管理画面にて、子カテゴリをチェックすると親カテゴリも自動的にチェック
* また、子カテゴリのチェックを外すと親カテゴリのチェックを外す
*/
/*
function category_parent_check_script() {
?>
<script>
jQuery(function($) {
  $('#taxonomy-category .children input').change(function() {
    function parentNodes(checked, nodes) {
      parents = nodes.parent().parent().parent().prev().children('input');
      if (parents.length != 0) {
        parents[0].checked = checked;
        parentNodes(checked, parents);
      }
    }
    var checked = $(this).is(':checked');
    $(this).parent().parent().siblings().children('label').children('input').each(function() {
      checked = checked || $(this).is(':checked');
    })
      parentNodes(checked, $(this));
  });
});
</script>
<?php
}
add_action('admin_head-post-new.php', 'category_parent_check_script');
add_action('admin_head-post.php', 'category_parent_check_script');
*/

/*==========================================================================================
* カスタム投稿タイプ・カスタムタクソノミー
*===========================================================================================
* ※※※　設定後、設定＞パーマリンク設定を開いて保存ボタンを押すことで反映されます　※※※
* 必要に応じて single-[post_type].php を作成
*/

/*------------------------------------------------------------
* カスタム投稿タイプ・カスタムタクソノミーを定義。複数定義することもできる。
*/
/*
function create_post_type_and_taxonomy() {
  // カスタム投稿タイプ名
  $post_type = 'temp_posttype';              // ◀◀◀カスタム投稿タイプの英名を設定

  //----------------------------------------
  // カスタムタクソノミー
  $tax = 'temp_tax';                         // ◀◀◀カスタムタクソノミーの英名を設定
  $taxes[] = $tax;
  $args = [
    'label' => '＜タクソノミー名＞',             // ◀◀◀カスタムタクソノミーの表示名を設定
    'public' => true,
    'hierarchical' => true, //階層を持たせるかどうか。trueならカテゴリ, falseならタグ型
    'show_admin_column' => true,
  ];
  register_taxonomy($tax, $post_type, $args);

  //----------------------------------------
  // カスタム投稿タイプ
  $args = [
    'label' => '＜投稿タイプ名＞',              // ◀◀◀カスタム投稿タイプの表示名を設定
    // 投稿画面に表示する項目
    'supports' => [
      // 記事タイトル、記事本文、アイキャッチ、リビジョン、カスタムフィールド
      'title', 'editor', 'thumbnail', 'revisions', 'custom-fields',
    ],
    'taxonomies' => $taxes,
    'public' => true, // この投稿タイプを有効にする
    'has_archive' => true, // アーカイブを有効にする
    'menu_position' => 5, // 管理画面の表示位置
  ];
  register_post_type($post_type, $args);
}
add_action( 'init', 'create_post_type_and_taxonomy' );

// 管理画面のカスタム投稿一覧に、カスタムタクソノミーの絞り込みをつける
function add_term_dropdown( $post_type ) {

  if ( $post_type === 'temp_posttype' ) {     // ◀◀◀カスタム投稿タイプ 'temp_posttype' の場合
    $taxonomy = 'temp_tax';                   // ◀◀◀カスタムタクソノミー 'temp_tax' の場合

    wp_dropdown_categories( array(
        'show_option_all'    => 'すべての'.get_taxonomy($taxonomy)->label,
        'selected'           => get_query_var( $taxonomy ), // 絞り込んだあとそのタームが選択されている状態を維持
        'hierarchical'       => true,
        'name'               => $taxonomy, // select の name 属性
        'taxonomy'           => $taxonomy, // カスタムタクソノミーのスラッグ
        'value_field'        => 'slug', // option の value属性の中身を何にするか
    ));
  }
}
add_action( 'restrict_manage_posts', 'add_term_dropdown');
*/

/*------------------------------------------------------------
* アーカイブページにて、複数のカスタム投稿タイプを混ぜて表示する
* 下記例) 投稿タイプ'post'に、投稿タイプ'post', 'info', 'product'を混ぜて表示
*/
/*
function pre_get_posts_custom( $query ) {
  if ( is_admin() || !$query->is_main_query() ) return;

  if ( $query->is_post_type_archive( 'post' ) && !$query->is_search() )
  {
    $query->set( 'post_type', array(
      // ここに混ぜて表示したい投稿タイプ名を列挙
      'post', 'info', 'product'
    ));
  }
}
add_action( 'pre_get_posts', 'pre_get_posts_custom' );
*/
